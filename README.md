# FediEkklesia

![](/fastlane/metadata/android/en-US/images/icon.png)

FediEkklesia is an Android client for [Mastodon](https://github.com/tootsuite/mastodon). Mastodon is a GNU social-compatible federated social network.

## Features

- Material Design
- Most Mastodon APIs implemented
- Multi-Account support
- Dark, light and black themes with the possibility to auto-switch based on the time of day
- Drafts - compose toots and save them for later
- Choose between different emoji styles 
- Optimized for all screen sizes
- Completely open-source - no non-free dependencies like Google services

